/**
 * Anayting cachable is a cacheItem.
 * Use this class to extent your objects Imag, Meta, Texture etc...
 */
define([
    'p!-eventful',
    'p!-assertful',
    'p!-logger',
    './error'
], function(
    $eventful,
    $assertful,
    $logger,
    $error
) {
    'use strict';

    var ImageClass;
    var DEFAULT_CROSS_ORIGIN = 'anonymous';
    var MAX_TRY = 2;
    var DEFAULT_COMPRESSION_RATIO = 8; //1:5
    var DEFAULT_URL = 'images/252x108.png';
    var DEFAULT_IMAGE = null;
    var DEFAULT_LOAD = true;
    var DEFAULT_SIZE = {
        width: 252,
        height: 108
    };

    var DEFAULT_CONFIG = {
        size: DEFAULT_SIZE,
        fallback: null,
        load: DEFAULT_LOAD,
        compressionRatio: DEFAULT_COMPRESSION_RATIO,
        crossOrigin: DEFAULT_CROSS_ORIGIN
    };

    /**
     * public function to get default url
     * @returns {String} default url
     */
    function getDefaultUrl() {
        return DEFAULT_URL;
    }

    /**
     * [transformURL description]
     * @param   {[type]} __url [description]
     * @returns {[type]}       [description]
     */
    function transformURL(__url) {
        return __url;
    }

    /**
     * Image loade try LIMI number
     * @returns {Number} 0 or Positive Int
     */
    function getTryCount() {
        return MAX_TRY;
    }

    /**
     * Sets image try load count
     * @param {Number} __value zero or any positive int
     */
    function setTryCount(__value) {
        if (!$assertful.zero() || !$assertful.pint()) {
            throw $error($error.TYPE.TYPE_ERROR, 'Only "0" or positive numbers allowed');
        }
        MAX_TRY = __value;
    }

    /**
     * Class API
     * @type {Object}
     */
    ImageClass = {
        /**
         * Sets up variables and assign arguments provided
         * Throws error if assocby key function is not provided
         * @param {String} __url image url
         * @param {Object} __options cache parameters
         * @param {Object} __throttler a throttler module that is used to add. instance of image into when a load requested
         */
        constructor: function(__url, __options, __throttler) {
            var _nOptions; // normalized options
            //constr super
            ImageClass.super.constructor.call(this);

            if (!$assertful.string(__url)) {
                throw $error($error.TYPE.TYPE_ERROR, 'invalid url', __url);
            }

            if (!$assertful.optObject(__options)) {
                throw $error($error.TYPE.TYPE_ERROR, 'invalid configuration', __url);
            }

            _nOptions = __options || {};
            //give the ability for the transformation of URL before loading image it
            this.url = transformURL(__url);
            //set image compression ratio (will be used to calculate SYSTEM memory allocation)
            this.compressionRatio = _nOptions.compressionRatio || DEFAULT_COMPRESSION_RATIO;
            //set cross origin rule
            this.crossOrigin = _nOptions.crossOrigin || DEFAULT_CROSS_ORIGIN;
            //set a fallback image
            //it will be used if current fails to load or image is asked while loading
            //WARNING - a fallback image must be an instance of Image Class
            this.fallbackImage = _nOptions.fallback;
            // set the requested size (used for scaling + preallocation if available)
            if (_nOptions.requestedSize) {
                this.requestedSize = {
                    width: _nOptions.requestedSize.width,
                    height: _nOptions.requestedSize.height
                };
            }

            //preallocate memory
            this.__updateDataSize__();
            //set initial load count
            this.__loadCount__ = 0;
            //set network throttler
            this.throttler = __throttler;

            this.loaded = false;
            this.loading = false;
            this.error = false;

            //trigger load if configured
            if ((_nOptions.hasOwnProperty('load') ? _nOptions.load : DEFAULT_LOAD) !== false) {
                this.load();
            }
        },

        deconstructor: function(__doNotEmiitEvent) {
            this.emit('deconstructed');
            ImageClass.super.deconstructor.call(this);
            this.error = false;
            this.loading = false;
            this.loaded = false;
            this.loadTimeEnd = null;
            this.loadDuration = null;
            this.loadTimeStart = null;
            this.gpuSize = 0;
            this.fileSize = 0;
            this.crossOrigin = null;
            this.compressionRatio = null;
            this.actualSize = null;
            this.scaledSize = null;
            this.requestedSize = null;
            this.url = null;
        },
        /**
         * Sets .__DOMelement__ (Image) to null thus throwing Object to be picked up by JSGC
         * Sets .gpuSize and .fileSize calues to "0"
         * Emits Event (dataUpdated) to notify interested parties of a change
         * @param   {Boolean} __doNotEmiitEvent if true - prevents Event (dataUpdated) from being emitted
         * @returns {Object}  this instance
         */
        clearData: function() {
            this.actualSize = null;
            this.scaledSize = null;
            this.requestedSize = null;
            this.__updateDataSize__();
            //
            if (this.__DOMelement__) {
                //clear handlers
                this.__DOMelement__.onload = null;
                this.__DOMelement__.onerror = null;
                this.__DOMelement__ = null;
            }

            if (this.isLoadCalled()) {
                this.emit('unloaded');
            }

            this.loaded = false;
            this.loading = false;

            return this;
        },

        /**
         * Triggers load without throttling the image load
         * However, if loaded or loading already - nothing is done.
         * @param  {Boolean} __force current load state bypass flag
         * @param {Boolean} __throttleBypass throttle bypass flag, if true - no throttling is done and load takes effect immediately
         * @returns {Boolean}         true if triggered load, else already loaded or currently loading
         */
        load: function(__force, __throttleBypass) {
            if (!this.isLoadable() && !__force) {
                return false;
            }
            //if throttle bypass flag is set, no throttling will be done
            if (this.throttler && __throttleBypass !== true) {
                this.throttler.add(this);
                return true;
            }
            this.__loadCount__ += 1;

            this.loadTimeStart = Date.now();

            this.loaded = false;
            //this.error = false;
            this.loading = true;

            this.error = false;

            this.__DOMelement__ = new Image();

            this.__DOMelement__.setAttribute('crossorigin', this.crossOrigin);

            this.__DOMelement__.onload = this.__onload__.bind(this, this.url);
            this.__DOMelement__.onerror = this.__onerror__.bind(this);
            //start Browser load
            this.__DOMelement__.src = this.url;
            this.emit('loading');
            return true;
        },

        /**
         * Validated arguments then calles a private method .__setSize__()
         * @param   {Number} __width  max value
         * @param   {Number} __height max value
         * @param {Boolean} __ignoreRatio if true - the original/requested size ratio will be ignored
         * @returns {Object}  this instance
         */
        setSize: function(__width, __height, __ignoreRatio) {
            if (!$assertful.pint(__width) || !$assertful.pint(__height) || !$assertful.optBoolean(__ignoreRatio)) {
                throw $error($error.TYPE.SIZE, 'Invalid size value');
            }
            return this.__setSize__(__width, __height, __ignoreRatio);
        },

        /**
         * Sets width/height property of this Object only to a sandbox values keeping original ratio intact if not explicitly ignored
         * Image will be scaled to point where one of the values is equal to provided arguments and
         * with the other value being less or equal to its designated argument
         * Outcome is size no exceeding niether argument values
         * Original image W/H will not be alterred. This would enable re-scaling image keeping original ratio
         * @param   {Number} __width  max value
         * @param   {Number} __height max value
         * @param   {Boolean} __ignoreRatio if true - the original/requested size ration will be ignored
         * @returns {Object}  this instance
         */
        __setSize__: function(__width, __height, __ignoreRatio) {
            var _ratioWidth, _ratioHeight;
            var _origin = this.actualSize || this.requestedSize;
            this.scaledSize = this.scaledSize || {};

            if (__ignoreRatio === true) {
                this.scaledSize.width = __width;
                this.scaledSize.height = __height;
                return this;
            }
            //make ratios
            if (isNaN(__width)) {
                _ratioWidth = 1;
            } else {
                _ratioWidth = __width / _origin.width;
            }
            if (isNaN(__height)) {
                _ratioHeight = 1;
            } else {
                _ratioHeight = __height / _origin.height;
            }

            //scale
            if (_ratioWidth < _ratioHeight) {
                this.scaledSize.width = _origin.width * _ratioWidth;
                this.scaledSize.height = _origin.height * _ratioWidth;
            } else {
                this.scaledSize.width = _origin.width * _ratioHeight;
                this.scaledSize.height = _origin.height * _ratioHeight;
            }

            return this;
        },

        /**
         * Evalueates arguments then calls private function __scale__
         * @param {Number} __number any number including negatives
         * @param   {Boolean} __original flag to use original values instaed of scaled values
         * @returns {Object}   this instance
         */
        scale: function(__number, __original) {
            if (!$assertful.number(__number) || !$assertful.optBoolean(__original)) {
                throw $error($error.TYPE.SIZE, 'Invalid scale value');
            }

            return this.__scale__(__number, __original);
        },

        /**
         * Scales width/height of either original values or scaled values
         * @param {Number} __number any number including negatives
         * @param   {Boolean} __original flag to use original values instaed of scaled values
         * @returns {Object}   this instance
         */
        __scale__: function(__number, __original) {
            return this.__setSize__(this.__getWidth__(__original) * __number, this.__getHeight__(__original) * __number);
        },

        /**
         * onload event handler
         * @param {String} __url original image URL - in case of override error will be reaised
         */
        __onload__: function(__url) {
            //record the actual size
            this.actualSize = {
                width: this.__DOMelement__.width,
                height: this.__DOMelement__.height
            };

            if (this.requestedSize) {
                //scale to a requested parameters
                this.__setSize__(this.requestedSize.width, this.requestedSize.height);
            } else {
                //don't scale
                this.__setSize__(this.actualSize.width, this.actualSize.height);
            }

            //track time
            this.loadTimeEnd = Date.now();
            this.loadDuration = this.loadTimeEnd - this.loadTimeStart;
            //set flags
            this.error = false;
            this.loading = false;
            this.loaded = true;
            //update size
            this.__updateDataSize__();

            this.emit('loaded');
        },

        /**
         * Assigns a data size based on dimensions of the image (width/height)
         * The raw pixel value of 32 bit/p is assigned as a gpu size
         * And then file sizes as a ratio of compression defined either locally or per instance is calculated
         * @event {dataSize} is emitted when data size is set
         */
        __updateDataSize__: function() {
            var _width, _height;
            if (this.actualSize) {
                _width = this.actualSize.width;
                _height = this.actualSize.height;
            } else if (this.requestedSize) {
                _width = this.requestedSize.width;
                _height = this.requestedSize.height;
            } else {
                _width = 0;
                _height = 0;
            }
            //check the files type
            this.gpuSize = _width * _height * 0.004; //exact memory footprint in KB: PX*32bits/8(to bytes)/1000(to KB)
            //JPG RATIO
            this.fileSize = this.gpuSize / this.compressionRatio; //compression ratio APPROX! KB
            //
            this.emit('dataSize');
        },

        /**
         * Returns native image element if assigned.
         * No other action performed
         * @returns {Object} Image Element or null
         */
        getData: function() {
            return this.__DOMelement__;
        },

        /**
         * returns a value of image size in KB of as compressed file to the ratio defined per instane
         * or globally
         * @returns {Number} file size in KB
         */
        getFileSize: function() {
            return this.fileSize;
        },

        /**
         * returns a value of image size in KB of as uncompressed file/texture
         * @returns {Number} raw size in KB
         */
        getGPUSize: function() {
            return this.gpuSize;
        },

        /**
         * Sets error state and triggers re-load()
         * @param {Object} __err Event object Error
         */
        __onerror__: function(__err) {
            this.reload();
        },

        /**
         * Calls load for set amount of tries
         */
        reload: function() {
            this.loading = false;
            this.loaded = false;
            if (this.__loadCount__ < MAX_TRY) {
                //load again without throttling
                this.load(true, true);
            } else if (!this.__isDefaultImage()) {
                //set error to true
                this.error = true;
                this.emit('error', $error($error.TYPE.LOAD, 'Fail to load image'));
            } else {
                throw $error($error.TYPE.DEFAULT, 'Default image was not loaded');
            }
        },

        /**
         * Load test that accounts for error state
         * @returns {Boolean} true if loading and no error
         */
        isLoading: function() {
            return !this.error && this.loading;
        },

        /**
         * Load test that accounts for error state
         * @returns {Boolean} true if loading and no error
         */
        isLoadCalled: function() {
            return !!this.loadTimeStart;
        },

        /**
         * Test for error and load states
         * loading:true, and loaded:true will indicate that the instance is not loadable, becaue its either loading or has already been loaded
         * @returns {Boolean} true is load can be performed
         */
        isLoadable: function() {
            return !this.error && !this.loading && !this.loaded;
        },

        /**
         * Validates arguments then calls private .__getWidth__()
         * if not passed the default false flag is used
         * @param {Boolean} __originalSize if true - returns original image width
         * @returns {Number} image width
         */
        getWidth: function(__originalSize) {
            if (!$assertful.optTrue(__originalSize)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Only booleans allowed'));
                return this.__getWidth__();
            }

            return this.__getWidth__(__originalSize);
        },

        /**
         * Assigned or derived from images data width
         * @param {Boolean} __originalSize if true - returns original image width
         * @returns {Number} image width
         */
        __getWidth__: function(__originalSize) {
            var _origin;
            if (__originalSize) {
                _origin = this.actualSize || this.scaledSize || this.requestedSize;
            } else {
                _origin = this.scaledSize || this.actualSize || this.requestedSize;
            }
            return _origin ? _origin.width : 0;
        },

        /**
         * Validates arguments then calls private .__getWidth__()
         * if not passed the default false flag is used
         * @param {Boolean} __originalSize if true - returns original image height
         * @returns {Number} image height
         */
        getHeight: function(__originalSize) {
            if (!$assertful.optTrue(__originalSize)) {
                this.emit('error', $error($error.TYPE.TYPE_ERROR, 'Only Booleans allowed'));
                return this.__getHeight__();
            }
            return this.__getHeight__(__originalSize);
        },

        /**
         * Assigned or derived from images data height
         * @param {Boolean} __originalSize if true - returns original image height
         * @returns {Number} image height
         */
        __getHeight__: function(__originalSize) {
            var _origin;
            if (__originalSize) {
                _origin = this.actualSize || this.scaledSize || this.requestedSize;
            } else {
                _origin = this.scaledSize || this.actualSize || this.requestedSize;
            }
            return _origin ? _origin.height : 0;
        },

        /**
         * Retuns a DOM Image Element if loaded,
         * Else returns Image Element of either fallback (defined by options) or
         * a DEFATULT Image Element (that was created during parse time of this class)
         * If this is a DEFAULT Element and was not yet loaded - error is thrown
         * For Each of the above cases (except DAFAULT) - the .load() is triggerred if image was requested, but was never loaded
         * @returns {Image} DOM Image ELement
         */
        getImage: function() {
            var _image;
            //notify usage with Event regardless of the outcome
            this.emit('used');

            if (this.loaded) {
                return this.__DOMelement__;
            }

            if (this.__isDefaultImage()) {
                throw $error($error.TYPE.DEFAULT, 'default Image was not defined or not loaded! Nothing to fallback to...');
            } else {
                //trigger load
                //This will take care of the .loading case and other cases
                this.load();
            }

            _image = this.fallbackImage || DEFAULT_IMAGE;
            //have to test the loaded state
            return _image.loaded ? _image.getImage() : null;
        },

        /**
         * tests if current instance is in fact a default (fallback) instanceof this class
         * @returns {Boolean} true is the instance equal to DEFAULT_IMAGE as a local variable
         */
        __isDefaultImage: function() {
            return this === DEFAULT_IMAGE;
        },

        /**
         * Test type image
         * @returns {Boolean} true, if original image is a portrait type (height > width)
         */
        isPortrait: function() {
            return this.getWidth(true) < this.getHeight(true);
        },

        /**
         * Test type image
         * @returns {Boolean} true if original image is a landscape type (width > height)
         */
        isLandscape: function() {
            return this.getWidth(true) > this.getHeight(true);
        },

        /**
         * Test type image
         * @returns {Boolean} true if original image is has equal sides (width = height)
         */
        isSquare: function() {
            return !this.isPortrait() && !this.isLandscape();
        },

        /**
         * An abstruct for render engines
         * @returns {Image} DOM Element
         */
        getRenderableData: function() {
            return this.getImage();
        }
    };


    ImageClass = $eventful.emitter.subclass('Image', ImageClass);

    DEFAULT_IMAGE = ImageClass(DEFAULT_URL);

    ImageClass.transformURL = transformURL;
    ImageClass.getDefaultUrl = getDefaultUrl;
    ImageClass.DEFAULT_URL = DEFAULT_URL;
    ImageClass.DEFAULT_IMAGE = DEFAULT_IMAGE;
    ImageClass.getTryCount = getTryCount;
    ImageClass.setTryCount = setTryCount;
    ImageClass.DEFAULT_CONFIG = DEFAULT_CONFIG;
    return ImageClass;
});
