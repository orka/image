define(['p!-error'], function($error) {
    'use strict';

    var type = {
        LOAD: 'LOAD_ERROR',
        DEFAULT: 'DEFAULT',
        OPTIONS: 'OPTIONS',
        SIZE: 'SIZE'
    };

    var ErrorClass = $error.subclass('ImageError', {
        constructor: function(__type, __message) {
            ErrorClass.super.constructor.apply(this, arguments);
        },
        deconstructor: function() {
            ErrorClass.super.deconstructor.apply(this, arguments);
        },
        TYPE: type
    });

    ErrorClass.TYPE = type;
    return Object.freeze(ErrorClass);
});
