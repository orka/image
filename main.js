define(['p!-image/image', 'p!-image/error'], function($image, $error) {
    'use strict';
    return {
        image: $image,
        error: $error
    };
});
