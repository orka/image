/*
Construction of image instances test
 */
define(['../lib/image'], function($image) {
    'use strict';
    //
    function url() {
        return 'images/252x108.png?' + Math.random();
    }
    //
    function create(__load) {
        return $image(url(), {
            load: __load
        });
    }
    //not loading
    describe('If Image "load" option equal "false"', function() {
        var image;
        beforeEach(function() {
            image = create(false);
        });
        //
        it('Should have .url attribute be defined', function() {
            expect(image.url).toBeDefined();
        });
        //
        it('Should have .loading attribute be "true"', function() {
            expect(image.loading).toEqual(false);
        });
        it('Should have .isLoading() return "false"', function() {
            expect(image.isLoading()).toEqual(false);
        });
        //isLoadCalled
        it('Should have .isLoadCalled() return "false"', function() {
            expect(image.isLoadCalled()).toEqual(false);
        });
        //isLoadable
        it('Should have .isLoadable() return "true"', function() {
            expect(image.isLoadable()).toEqual(true);
        });
        //
        it('Should have .loaded to equal "false"', function() {
            expect(image.loaded).toEqual(false);
        });
        //this.__DOMelement__
        it('Should NOW have private .__DOMelement__ to be defined', function() {
            expect(image.__DOMelement__).not.toBeDefined();
        });
        //this.error
        it('Should have .error to be equal "false"', function() {
            expect(image.error).toEqual(false);
        });
        //this.loadTimeStart
        it('Should not have .loadTimeStart defined', function() {
            expect(image.loadTimeStart).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .loadTimeEnd defined', function() {
            expect(image.loadTimeEnd).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .loadDuration defined', function() {
            expect(image.loadDuration).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .throttler defined', function() {
            expect(image.throttler).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should have .__loadCount__ to equal to "0"', function() {
            expect(image.__loadCount__).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .crossOrigin to equal to "anonymous"', function() {
            expect(image.crossOrigin).toEqual('anonymous');
        });
        //this.loadTimeStart
        it('Should not have .fallbackImage defined', function() {
            expect(image.fallbackImage).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .actualSize defined', function() {
            expect(image.actualSize).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .requestedSize defined', function() {
            expect(image.requestedSize).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should not have .scaledSize defined', function() {
            expect(image.scaledSize).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should have .getData() return .__DOMelement__', function() {
            expect(image.getData()).not.toBeDefined();
        });
        //this.loadTimeStart
        it('Should have .getFileSize() return "0"', function() {
            expect(image.getFileSize()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getGPUSize() return "0"', function() {
            expect(image.getGPUSize()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getWidth() return "0"', function() {
            expect(image.getWidth()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getHeight() return "0"', function() {
            expect(image.getHeight()).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getWidth(true) return "0"', function() {
            expect(image.getWidth(true)).toEqual(0);
        });
        //this.loadTimeStart
        it('Should have .getHeight(true) return "0"', function() {
            expect(image.getHeight(true)).toEqual(0);
        });
        //this.loadTimeStart
        /*it('Should have .getImage() equal null', function() {
            expect(image.getImage() === $image.DEFAULT_IMAGE.getImage()).toEqual(true);
        });*/

        //this.loadTimeStart
        it('Should have .__isDefaultImage() return "false"', function() {
            expect(image.__isDefaultImage()).toEqual(false);
        });
        //this.loadTimeStart
        it('Should have .getRenderableData() return .getImage() value', function() {
            expect(image.getRenderableData()).toEqual(image.getImage());
        });
    });
    //loading and loaded
    describe('If Image "load" option not equal "false"', function() {
        //
        describe('While in "loading" state', function() {
            var image;
            beforeEach(function() {
                image = create();
            });
            //
            it('Should have .url attribute be defined', function() {
                expect(image.url).toBeDefined();
            });
            //
            it('Should have .loading attribute be true', function() {
                expect(image.loading).toEqual(true);
            });
            it('Should have .isLoading() return true', function() {
                expect(image.isLoading()).toEqual(true);
            });
            //isLoadCalled
            it('Should have .isLoadCalled() return true', function() {
                expect(image.isLoadCalled()).toEqual(true);
            });
            //isLoadable
            it('Should have .isLoadable() return false', function() {
                expect(image.isLoadable()).toEqual(false);
            });
            //
            it('Should have .loaded to not equal true', function() {
                expect(image.loaded).not.toEqual(true);
            });
            //this.__DOMelement__
            it('Should have private .__DOMelement__ to be defined', function() {
                expect(image.__DOMelement__).toBeDefined();
            });
            //this.error
            it('Should have .error to not be equal "false"', function() {
                expect(image.error).not.toEqual(true);
            });
            //this.loadTimeStart
            it('Should have .loadTimeStart defined', function() {
                expect(image.loadTimeStart).toBeDefined();
                expect(image.loadTimeStart).toBeGreaterThan(0);
            });
            //this.loadTimeStart
            it('Should not have .loadTimeEnd defined', function() {
                expect(image.loadTimeEnd).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .loadDuration defined', function() {
                expect(image.loadDuration).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .throttler defined', function() {
                expect(image.throttler).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should have .__loadCount__ to equal to "1"', function() {
                expect(image.__loadCount__).toEqual(1);
            });
            //this.loadTimeStart
            it('Should have .crossOrigin to equal to "anonymous"', function() {
                expect(image.crossOrigin).toEqual('anonymous');
            });
            //this.loadTimeStart
            it('Should not have .fallbackImage defined', function() {
                expect(image.fallbackImage).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .actualSize defined', function() {
                expect(image.actualSize).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .requestedSize defined', function() {
                expect(image.requestedSize).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should not have .scaledSize defined', function() {
                expect(image.scaledSize).not.toBeDefined();
            });
            //this.loadTimeStart
            it('Should have .getData() return .__DOMelement__', function() {
                expect(image.getData()).toEqual(image.__DOMelement__);
            });
            //this.loadTimeStart
            it('Should have .getFileSize() return "0"', function() {
                expect(image.getFileSize()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getGPUSize() return "0"', function() {
                expect(image.getGPUSize()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getWidth() return "0"', function() {
                expect(image.getWidth()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getHeight() return "0"', function() {
                expect(image.getHeight()).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getWidth(true) return "0"', function() {
                expect(image.getWidth(true)).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getHeight(true) return "0"', function() {
                expect(image.getHeight(true)).toEqual(0);
            });
            //this.loadTimeStart
            it('Should have .getImage() not equal .getData() - due to not loaded state', function() {
                expect(image.getImage()).not.toEqual(image.getData());
            });
            //this.loadTimeStart
            // it('Should have .getImage() return "null" - due to no default or fallback image loaded state', function() {
            //     expect(image.getImage()).toEqual(null);
            // });
            //this.loadTimeStart
            it('Should have .__isDefaultImage() return "false"', function() {
                expect(image.__isDefaultImage()).toEqual(false);
            });
            //this.loadTimeStart
            it('Should have .getRenderableData() return .getImage() value', function() {
                expect(image.getRenderableData()).toEqual(image.getImage());
            });
        });

        describe('When loaded (set on "loaded" event)', function() {
            //
            var image;
            //set up fpr each
            beforeEach(function(done) {
                image = create();
                image.addListener('loaded', done);
            });
            //
            it('Should have .url attribute be defined', function(done) {
                expect(image.url).toBeDefined();
                done();
            });
            //
            it('Should have .loading attribute be false', function(done) {
                expect(image.loading).toEqual(false);
                done();
            });
            //
            it('Should have .isLoading() return false', function(done) {
                expect(image.isLoading()).toEqual(false);
                done();
            });
            //isLoadCalled
            it('Should have .isLoadCalled() return true', function(done) {
                expect(image.isLoadCalled()).toEqual(true);
                done();
            });
            //isLoadable
            it('Should have .isLoadable() return false', function(done) {
                expect(image.isLoadable()).toEqual(false);
                done();
            });
            //
            it('Should have .loaded to equal true', function(done) {
                expect(image.loaded).toEqual(true);
                done();
            });
            //this.__DOMelement__
            it('Should have private .__DOMelement__ to be defined', function(done) {
                expect(image.__DOMelement__).toBeDefined();
                done();
            });
            //this.error
            it('Should have .error to not be equal "true"', function(done) {
                expect(image.error).not.toEqual(true);
                done();
            });
            //this.loadTimeStart
            it('Should have .loadTimeStart defined', function(done) {
                expect(image.loadTimeStart).toBeDefined();
                expect(image.loadTimeStart).toBeGreaterThan(0);
                done();
            });
            //this.loadTimeEnd
            it('Should have .loadTimeEnd defined and be more than "0"', function(done) {
                expect(image.loadTimeEnd).toBeDefined();
                expect(image.loadTimeEnd).toBeGreaterThan(0);
                done();
            });
            //this.loadDuration
            it('Should have .loadDuration defined and be more than "0"', function(done) {
                expect(image.loadDuration).toBeDefined();
                expect(image.loadDuration).toBeGreaterThan(0);
                done();
            });
            //this.__loadCount__
            it('Should have .__loadCount__ to equal to "1"', function(done) {
                expect(image.__loadCount__).toEqual(1);
                done();
            });
            //this.crossOrigin
            it('Should have .crossOrigin to equal to "anonymous"', function(done) {
                expect(image.crossOrigin).toEqual('anonymous');
                done();
            });
            //this.actualSize
            it('Should have .actualSize defined and be equal to image.width, image.height', function(done) {
                expect(image.actualSize).toBeDefined();
                expect(image.actualSize.width).toEqual(image.getData().width);
                expect(image.actualSize.height).toEqual(image.getData().height);
                done();
            });
            //this.requestedSize
            it('Should not have .requestedSize defined', function(done) {
                expect(image.requestedSize).not.toBeDefined();
                done();
            });
            //this.scaledSize
            it('Should have .scaledSize defined', function(done) {
                expect(image.scaledSize).toBeDefined();
                done();
            });
            //this.getData()
            it('Should have .getData() return .__DOMelement__', function(done) {
                expect(image.getData()).toEqual(image.__DOMelement__);
                done();
            });
            //this.getFileSize()
            it('Should have .getFileSize() return actual(width*height)*0.004/compression', function(done) {
                expect(image.getFileSize()).toEqual(image.getData().width * image.getData().height * 0.004 / image.compressionRatio);
                done();
            });
            //this.getGPUSize()
            it('Should have .getGPUSize() return actual(width*height)*0.004', function(done) {
                expect(image.getGPUSize()).toEqual(image.getData().width * image.getData().height * 0.004);
                done();
            });
            //this.getWidth()
            it('Should have .getWidth() element width', function(done) {
                expect(image.getWidth()).toEqual(image.getData().width);
                done();
            });

            //this.getHeight()
            it('Should have .getHeight() element height', function(done) {
                expect(image.getHeight()).toEqual(image.getData().height);
                done();
            });
            //this.getWidth(true)
            it('Should have .getWidth(true) element width', function(done) {
                expect(image.getWidth(true)).toEqual(image.getData().width);
                done();
            });
            //this.getHeight(true)
            it('Should have .getHeight(true) element height', function(done) {
                expect(image.getHeight(true)).toEqual(image.getData().height);
                done();
            });

            //this.getImage()
            it('Should have .getImage() equal to .getData() - due to loaded state', function(done) {
                expect(image.getImage()).toEqual(image.getData());
                done();
            });
            //this.getImage()
            it('Should have .getImage() return .__DOMelement__', function(done) {
                expect(image.getImage()).toEqual(image.__DOMelement__);
                done();
            });
            //this.__isDefaultImage()
            it('Should have .__isDefaultImage() return "false"', function(done) {
                expect(image.__isDefaultImage()).toEqual(false);
                done();
            });
            //this.getRenderableData()
            it('Should have .getRenderableData() return .getImage() value', function(done) {
                expect(image.getRenderableData()).toEqual(image.getImage());
                done();
            });

            it('Should have .getRenderableData() return instance of Image', function(done) {
                expect(image.getRenderableData() instanceof Image).toEqual(true);
                done();
            });
        });
    });
});
