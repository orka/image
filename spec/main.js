define([
    'require',
    'jasmine-boot'
], function (
    require
) {
    'use strict';

    require([
        './createImage',
        './loadImage',
        './loadError',
        './setSize'
    ], function () {
        window.onload();
    });
});
