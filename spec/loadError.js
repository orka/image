/*
Construction of image instances test
 */
define(['../lib/image'], function($image) {
    'use strict';
    //
    function url() {
        return 'non-existent image?' + Math.random();
    }
    //
    function create(__load) {
        return $image(url(), {
            load: __load
        });
    }

    describe('When 404 and "load" option not equal "false" ', function() {
        //
        var image;
        //set up for each
        beforeEach(function(done) {
            image = create();
            image.addListener('error', done);
        });
        //
        it('Should have .error attribute be "true"', function(done) {
            expect(image.error).toEqual(true);
            done();
        });

        it('Should have .__loadCount__ attribute equal: ' + $image.getTryCount(), function(done) {
            expect(image.__loadCount__).toEqual($image.getTryCount());
            done();
        });

        it('Should have .url attribute be defined', function(done) {
            expect(image.url).toBeDefined();
            done();
        });
        //
        it('Should have .loading attribute be true', function(done) {
            expect(image.loading).toEqual(false);
            done();
        });
        //
        it('Should have .isLoading() return true', function(done) {
            expect(image.isLoading()).toEqual(false);
            done();
        });
        //isLoadCalled
        it('Should have .isLoadCalled() return true', function(done) {
            expect(image.isLoadCalled()).toEqual(true);
            done();
        });
        //isLoadable
        it('Should have .isLoadable() return false', function(done) {
            expect(image.isLoadable()).toEqual(false);
            done();
        });
        //
        it('Should have .loaded to equal "false"', function(done) {
            expect(image.loaded).toEqual(false);
            done();
        });
        //this.__DOMelement__
        it('Should have private .__DOMelement__ to be defined', function(done) {
            expect(image.__DOMelement__).toBeDefined();
            done();
        });
        //this.error
        it('Should have .error to be equal "true"', function(done) {
            expect(image.error).toEqual(true);
            done();
        });
        //this.loadTimeStart
        it('Should have .loadTimeStart defined', function(done) {
            expect(image.loadTimeStart).toBeDefined();
            expect(image.loadTimeStart).toBeGreaterThan(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .loadTimeEnd not to be defined', function(done) {
            expect(image.loadTimeEnd).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should have .loadDuration not to be defined', function(done) {
            expect(image.loadDuration).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should have .crossOrigin to equal to "anonymous"', function(done) {
            expect(image.crossOrigin).toEqual('anonymous');
            done();
        });
        //this.loadTimeStart
        it('Should have .actualSize not to be defined', function(done) {
            expect(image.actualSize).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should not have .requestedSize defined', function(done) {
            expect(image.requestedSize).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should not have .scaledSize defined', function(done) {
            expect(image.scaledSize).not.toBeDefined();
            done();
        });
        //this.loadTimeStart
        it('Should have .getData() return .__DOMelement__', function(done) {
            expect(image.getData()).toEqual(image.__DOMelement__);
            done();
        });
        //this.loadTimeStart
        it('Should have .getFileSize() return "0"', function(done) {
            expect(image.getFileSize()).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getGPUSize() return "0"', function(done) {
            expect(image.getGPUSize()).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getWidth() "0"', function(done) {
            expect(image.getWidth()).toEqual(0);
            done();
        });

        //this.loadTimeStart
        it('Should have .getHeight() "0"', function(done) {
            expect(image.getHeight()).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getWidth(true) "0"', function(done) {
            expect(image.getWidth(true)).toEqual(0);
            done();
        });
        //this.loadTimeStart
        it('Should have .getHeight(true) "0"', function(done) {
            expect(image.getHeight(true)).toEqual(0);
            done();
        });

        //this.loadTimeStart
        it('Should have .getImage() not equal to .getData() - due to error state', function(done) {
            expect(image.getImage()).not.toEqual(image.getData());
            done();
        });
        //this.loadTimeStart
        it('Should have .getImage() return "DEFAULT_IMAGE"', function(done) {
            expect(image.getImage() === $image.DEFAULT_IMAGE.getImage()).toEqual(true);
            done();
        });
        //this.loadTimeStart
        it('Should have .__isDefaultImage() return "false"', function(done) {
            expect(image.__isDefaultImage()).toEqual(false);
            done();
        });
        //this.loadTimeStart
        it('Should have .getRenderableData() return .getImage() value', function(done) {
            expect(image.getRenderableData()).toEqual(image.getImage());
            done();
        });

        it('Should have .getRenderableData() return "DEFAULT_IMAGE"', function(done) {
            expect(image.getRenderableData() === $image.DEFAULT_IMAGE.getRenderableData()).toEqual(true);
            done();
        });
    });
});
