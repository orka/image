/*
Construction of image instances test
 */
define(['../lib/image'], function($image) {
    'use strict';
    var moreSize = {
        width: 500,
        height: 500
    };

    var lessSize = {
        width: 10,
        height: 10
    };

    var trueSize = {
        width: 252,
        height: 108
    };

    var biggerWidthSize = {
        width: 500,
        height: 108
    };

    var biggerWidthSmallerHeightSize = {
        width: 500,
        height: 10
    };

    var biggerHeightSmallerWidthSize = {
        width: 10,
        height: 500
    };

    var biggerHeightSize = {
        width: 252,
        height: 500
    };

    //
    function url() {
        return 'images/252x108.png?' + Math.random();
    }

    //
    function create(__size) {
        return $image(url(), {
            requestedSize: __size
        });
    }

    function testRatio(image, size) {
        var _ratioWidth = size.width / image.actualSize.width;
        var _ratioHeight = size.height / image.actualSize.height;

        if (_ratioWidth < _ratioHeight) {
            expect(image.getWidth()).toEqual(size.width);
            expect(image.getHeight()).toEqual(image.actualSize.height * _ratioWidth);
        } else {
            expect(image.getWidth()).toEqual(image.actualSize.width * _ratioHeight);
            expect(image.getHeight()).toEqual(size.height);
        }
    }
    //not loading
    describe('Requesting a true Image size', function() {
        var image;
        describe('while not loading, not loaded', function() {
            beforeEach(function() {
                image = $image(url(), {
                    requestedSize: trueSize,
                    load: false
                });
            });
            //
            it('Should have .requestedSize defined', function() {
                expect(image.requestedSize).toBeDefined();
            });
            it('Should not have .requestedSize equal to options.size object', function() {
                expect(image.requestedSize === trueSize).toEqual(false);
            });
            it('Should have .requestedSize.width to equal: "' + trueSize.width + '"', function() {
                expect(image.requestedSize.width).toEqual(trueSize.width);
            });
            it('Should have .requestedSize.height to equal: "' + trueSize.height + '"', function() {
                expect(image.requestedSize.height).toEqual(trueSize.height);
            });
            it('Should have .getWidth() return: "' + trueSize.width + '"', function() {
                expect(image.getWidth()).toEqual(trueSize.width);
            });
            it('Should have .getHeight() return: "' + trueSize.height + '"', function() {
                expect(image.getHeight()).toEqual(trueSize.height);
            });
            it('Should have .getWidth(true) return: "' + trueSize.width + '"', function() {
                expect(image.getWidth(true)).toEqual(trueSize.width);
            });
            it('Should have .getHeight(true) return: "' + trueSize.height + '"', function() {
                expect(image.getHeight(true)).toEqual(trueSize.height);
            });
        });
        //
        describe('while loading', function() {
            beforeEach(function() {
                image = create(trueSize);
            });
            //
            it('Should have .requestedSize defined', function() {
                expect(image.requestedSize).toBeDefined();
            });
            it('Should not have .requestedSize equal to options.size object by reference', function() {
                expect(image.requestedSize === trueSize).toEqual(false);
            });
            it('Should have .requestedSize.width to equal: "' + trueSize.width + '"', function() {
                expect(image.requestedSize.width).toEqual(trueSize.width);
            });
            it('Should have .requestedSize.height to equal: "' + trueSize.height + '"', function() {
                expect(image.requestedSize.height).toEqual(trueSize.height);
            });
            it('Should have .getWidth() return: "' + trueSize.width + '"', function() {
                expect(image.getWidth()).toEqual(trueSize.width);
            });
            it('Should have .getHeight() return: "' + trueSize.height + '"', function() {
                expect(image.getHeight()).toEqual(trueSize.height);
            });
            it('Should have .getWidth(true) return: "' + trueSize.width + '"', function() {
                expect(image.getWidth(true)).toEqual(trueSize.width);
            });
            it('Should have .getHeight(true) return: "' + trueSize.height + '"', function() {
                expect(image.getHeight(true)).toEqual(trueSize.height);
            });
        });

        describe('When loaded', function() {
            beforeEach(function(done) {
                image = create(trueSize);
                image.addListener('loaded', done);
            });
            //
            it('Should have .requestedSize defined', function(done) {
                expect(image.requestedSize).toBeDefined();
                done();
            });
            it('Should not have .requestedSize equal to options.size object', function(done) {
                expect(image.requestedSize === trueSize).toEqual(false);
                done();
            });
            it('Should have .requestedSize.width to equal: "' + trueSize.width + '"', function(done) {
                expect(image.requestedSize.width).toEqual(trueSize.width);
                done();
            });
            it('Should have .requestedSize.height to equal: "' + trueSize.height + '"', function(done) {
                expect(image.requestedSize.height).toEqual(trueSize.height);
                done();
            });
            it('Should have .actualSize.width to equal: "' + trueSize.width + '"', function(done) {
                expect(image.actualSize.width).toEqual(trueSize.width);
                done();
            });
            it('Should have .actualSize.height to equal: "' + trueSize.height + '"', function(done) {
                expect(image.actualSize.height).toEqual(trueSize.height);
                done();
            });
            it('Should have .getWidth() return: "' + trueSize.width + '"', function() {
                expect(image.getWidth()).toEqual(trueSize.width);
            });
            it('Should have .getHeight() return: "' + trueSize.height + '"', function() {
                expect(image.getHeight()).toEqual(trueSize.height);
            });
            it('Should have .getWidth(true) return: "' + trueSize.width + '"', function() {
                expect(image.getWidth(true)).toEqual(trueSize.width);
            });
            it('Should have .getHeight(true) return: "' + trueSize.height + '"', function() {
                expect(image.getHeight(true)).toEqual(trueSize.height);
            });
        });
    });

    describe('Set Image size', function() {
        var image;

        describe('Testing arguments', function() {
            beforeEach(function() {
                image = $image(url(), {
                    requestedSize: trueSize
                });
            });
            //
            it('Should have .setSize defined', function() {
                expect(image.setSize).toBeDefined();
            });

            it('Should have .setSize() throw error if either width or height is null or undefined', function() {
                expect(image.setSize.bind(image)).toThrow();
                expect(image.setSize.bind(image, null)).toThrow();
                expect(image.setSize.bind(image, undefined, null)).toThrow();
                expect(image.setSize.bind(image, null, null)).toThrow();
            });

            it('Should have .setSize() throw error if either width or height is not a number', function() {
                expect(image.setSize.bind(image, {}, {})).toThrow();
                expect(image.setSize.bind(image, [], [])).toThrow();
                expect(image.setSize.bind(image, function() {})).toThrow();
                expect(image.setSize.bind(image, -Infinity, Infinity)).toThrow();
                expect(image.setSize.bind(image, Infinity, -Infinity)).toThrow();
                expect(image.setSize.bind(image, '', '')).toThrow();
                expect(image.setSize.bind(image, '500', '500')).toThrow();
                expect(image.setSize.bind(image, '500', 500)).toThrow();
                expect(image.setSize.bind(image, 500)).toThrow();
                expect(image.setSize.bind(image, 500, 1.1)).toThrow();
                expect(image.setSize.bind(image, -2, 2)).toThrow();
                expect(image.setSize.bind(image, 0, 2)).toThrow();
            });
        });

        describe('while not loading, not loaded', function() {
            beforeEach(function() {
                image = $image(url(), {
                    requestedSize: trueSize,
                    load: false
                });
            });

            it('Should not throw error if width and height is a positive number', function() {
                expect(image.setSize.bind(image, 2, 2)).not.toThrow();
            });
            //
            it('Should not change .requestedSize', function() {
                image.setSize(1, 1);
                expect(image.requestedSize.width).toEqual(trueSize.width);
                expect(image.requestedSize.height).toEqual(trueSize.height);
            });
            it('Should not have .actualSize defined', function() {
                image.setSize(1, 1);
                expect(image.actualSize).not.toBeDefined();
            });
            it('Should set .scaledSize to new values', function() {
                image.setSize(10, 10);
                expect(image.getWidth()).not.toEqual(trueSize.width);
                expect(image.getHeight()).not.toEqual(trueSize.height);
            });
            it('Should set .scaledSize to new values with ratio bypass flag', function() {
                image.setSize(10, 10, true);
                expect(image.getWidth()).toEqual(10);
                expect(image.getHeight()).toEqual(10);
            });
        });

        describe('while 404 .error', function() {
            beforeEach(function(done) {
                image = $image('asd', {
                    requestedSize: trueSize
                });
                image.addListener('error', done);
            });

            it('Should not throw error if width and height is a positive number', function(done) {
                expect(image.setSize.bind(image, 2, 2)).not.toThrow();
                done();
            });
            //
            it('Should not change .requestedSize', function(done) {
                image.setSize(1, 1);
                expect(image.requestedSize.width).toEqual(trueSize.width);
                expect(image.requestedSize.height).toEqual(trueSize.height);
                done();
            });
            it('Should not have .actualSize defined', function(done) {
                image.setSize(1, 1);
                expect(image.actualSize).not.toBeDefined();
                done();
            });
            it('Should set .scaledSize to new values', function(done) {
                image.setSize(10, 10);
                expect(image.getWidth()).not.toEqual(trueSize.width);
                expect(image.getHeight()).not.toEqual(trueSize.height);
                done();
            });
            it('Should set .scaledSize to new values with ratio bypass flag', function(done) {
                image.setSize(10, 10, true);
                expect(image.getWidth()).toEqual(10);
                expect(image.getHeight()).toEqual(10);
                done();
            });
        });

        describe('while loading', function() {
            beforeEach(function() {
                image = $image(url(), {
                    requestedSize: trueSize
                });
            });

            it('Should not throw error if width and height is a positive number', function() {
                expect(image.setSize.bind(image, 2, 2)).not.toThrow();
            });
            //
            it('Should not change .requestedSize', function() {
                image.setSize(1, 1);
                expect(image.requestedSize.width).toEqual(trueSize.width);
                expect(image.requestedSize.height).toEqual(trueSize.height);
            });
            it('Should not have .actualSize defined', function() {
                image.setSize(1, 1);
                expect(image.actualSize).not.toBeDefined();
            });
            it('Should set .scaledSize to new values', function() {
                image.setSize(10, 10);
                expect(image.getWidth()).not.toEqual(trueSize.width);
                expect(image.getHeight()).not.toEqual(trueSize.height);
            });
            it('Should set .scaledSize to new values with ratio bypass flag', function() {
                image.setSize(10, 10, true);
                expect(image.getWidth()).toEqual(10);
                expect(image.getHeight()).toEqual(10);
            });
        });

        describe('When loaded calling .setSize()', function() {
            beforeEach(function(done) {
                image = $image(url(), {
                    requestedSize: trueSize
                });
                image.addListener('loaded', done);
            });

            it('Should not throw error if width and height is a positive number', function(done) {
                expect(image.setSize.bind(image, 2, 2)).not.toThrow();
                done();
            });
            //
            it('Should not change .requestedSize', function(done) {
                image.setSize(1, 1);
                expect(image.requestedSize.width).toEqual(trueSize.width);
                expect(image.requestedSize.height).toEqual(trueSize.height);
                done();
            });
            it('Should have .actualSize defined', function(done) {
                image.setSize(1, 1);
                expect(image.actualSize).toBeDefined();
                done();
            });
            it('Should have .actualSize be equal to: ' + trueSize.width + 'x' + trueSize.height, function(done) {
                image.setSize(1, 1);
                expect(image.actualSize.width).toEqual(trueSize.width);
                expect(image.actualSize.height).toEqual(trueSize.height);
                done();
            });
            it('Should set .scaledSize to new values', function(done) {
                image.setSize(10, 10);
                expect(image.getWidth()).not.toEqual(trueSize.width);
                expect(image.getHeight()).not.toEqual(trueSize.height);
                done();
            });


            it('Should set .scaledSize to new values - bigger than original retaining ratio', function(done) {
                image.setSize(moreSize.width, moreSize.height);
                testRatio(image, moreSize);
                done();
            });

            it('Should set .scaledSize to new values - smaller than original retaining ratio', function(done) {
                image.setSize(lessSize.width, lessSize.height);
                testRatio(image, lessSize);
                done();
            });

            it('Should set .scaledSize to new values - with height smaller than width retaining ratio', function(done) {
                image.setSize(biggerWidthSize.width, biggerWidthSize.height);
                testRatio(image, biggerWidthSize);
                done();
            });

            it('Should set .scaledSize to new values - with width smaller than height retaining ratio', function(done) {
                image.setSize(biggerHeightSize.width, biggerHeightSize.height);
                testRatio(image, biggerHeightSize);
                done();
            });

            it('Should set .scaledSize to new values - with width bigger than height that smaller than original height retaining ratio', function(done) {
                image.setSize(biggerWidthSmallerHeightSize.width, biggerWidthSmallerHeightSize.height);
                testRatio(image, biggerWidthSmallerHeightSize);
                done();
            });

            it('Should set .scaledSize to new values - with height bigger than width that smaller than original width retaining ratio', function(done) {
                image.setSize(biggerHeightSmallerWidthSize.width, biggerHeightSmallerWidthSize.height);
                testRatio(image, biggerHeightSmallerWidthSize);
                done();
            });

            it('Should set .scaledSize to new values with ratio bypass flag', function(done) {
                image.setSize(10, 10, true);
                expect(image.getWidth()).toEqual(10);
                expect(image.getHeight()).toEqual(10);
                done();
            });
        });

        describe('When loaded calling .scale()', function() {
            beforeEach(function(done) {
                image = $image(url(), {
                    requestedSize: trueSize
                });
                image.addListener('loaded', done);
            });

            it('Should not throw error if number provided', function(done) {
                expect(image.scale.bind(image, 1)).not.toThrow();
                done();
            });

            it('Should throw error if scale is not a number', function(done) {
                expect(image.scale.bind(image)).toThrow();
                expect(image.scale.bind(image, null)).toThrow();
                expect(image.scale.bind(image, [])).toThrow();
                expect(image.scale.bind(image, {})).toThrow();
                expect(image.scale.bind(image, Infinity)).toThrow();
                expect(image.scale.bind(image, -Infinity)).toThrow();
                done();
            });
            //
            it('Should not change .requestedSize', function(done) {
                image.scale(2);
                expect(image.requestedSize.width).toEqual(trueSize.width);
                expect(image.requestedSize.height).toEqual(trueSize.height);
                done();
            });
            it('Should have .actualSize defined', function(done) {
                image.scale(2);
                expect(image.actualSize).toBeDefined();
                done();
            });
            it('Should have .actualSize be equal to: ' + trueSize.width + 'x' + trueSize.height, function(done) {
                image.scale(2);
                expect(image.actualSize.width).toEqual(trueSize.width);
                expect(image.actualSize.height).toEqual(trueSize.height);
                done();
            });

            it('Should set .scaledSize to new values', function(done) {
                image.scale(1.5);
                expect(image.getWidth()).toEqual(trueSize.width * 1.5);
                expect(image.getHeight()).toEqual(trueSize.height * 1.5);
                done();
            });

            it('Should set .scaledSize to new values of original dimentions width flag ON and calling multiple times', function(done) {
                image.scale(1.5, true);
                image.scale(1.5, true);
                image.scale(1.5, true);
                expect(image.getWidth()).toEqual(trueSize.width * 1.5);
                expect(image.getHeight()).toEqual(trueSize.height * 1.5);
                done();
            });

            it('Should set .scaledSize to new values of scaled dimentions width flag OFF and calling multiple times', function(done) {
                image.scale(1.5);
                image.scale(1.5);
                image.scale(1.5);
                expect(image.getWidth()).toEqual(trueSize.width * 1.5 * 1.5 * 1.5);
                expect(image.getHeight()).toEqual(trueSize.height * 1.5 * 1.5 * 1.5);
                done();
            });
        });
    });
});
